<?php
require_once('../simpletest/unit_tester.php');
require_once('test_reporter.php');
require_once('xml_test_case.php');
require_once('class/xml_parser.php');

class TestXmlParser extends XmlTestCase {
    function TestXmlParser() {
        $this->XmlTestCase('XmlParser Test');
    }
    
    function testParseXml() {
        $dir = dir('xml_files');
        while (false !== ($entry = $dir->read())) {
            $xml = $dir->path . '/' . $entry;
            $parser = new XmlParser($xml);
            if ( is_file($xml) ) {
                $error = $parser->getErrorMessages();
                if ( preg_match('/error/', $xml) ) {
                    $this->assertTrue($error, "%s parsing: $xml");
                } else {
                    $this->assertFalse($error, "%s parsing: $xml");
                }
            }
        }
    }
}

$test = &new TestXmlParser();
$test->run(new TestReporter());
echo $test->time . " seconds";
?>