<?php
class XmlTestCase extends UnitTestCase {
    var $start = 0;
    var $time = 0;
    var $end = 0;
    
    function setUp() {
        $this->start = microtime(true);
    }
    
    function tearDown() {
        $this->end = microtime(true);
        $this->time += $this->end - $this->start;
        echo "\n" . ($this->end - $this->start) . " seconds<hr />";
    }
    
}
?>